const loginIdValidator = new FieldValidator("txtLoginId", async function (val) {
  if (!val) {
    return "请填写账号";
  }
  const resp = await API.exists(val);
  if (resp.data) {
    return "该账号名已被占用，请重新填写";
  }
});

const nicknameIdValidator = new FieldValidator("txtNickname", async function (
  val
) {
  if (!val) {
    return "请填写昵称";
  }
});

const loginPwdValidator = new FieldValidator("txtLoginPwd", async function (
  val
) {
  if (!val) {
    return "请填写密码";
  }
});

const loginPwdComfirmValidator = new FieldValidator(
  "txtLoginPwdConfirm",
  async function (val) {
    if (!val) {
      return "请填写确认密码";
    }
    if (val !== loginPwdValidator.input.value) {
      return "两次密码不一致";
    }
  }
);

const form = $(".user-form");
form.onsubmit = async function (e) {
  e.preventDefault();
  const result = await FieldValidator.validate(
    loginIdValidator,
    nicknameIdValidator,
    loginPwdValidator,
    loginPwdComfirmValidator
  );
  if (!result) {
    return; //验证未通过
  }
  const resp = await API.reg({
    loginId: loginIdValidator.input.value,
    loginPwd: loginPwdValidator.input.value,
    nickname: nicknameIdValidator.input.value,
  });
  if (resp.code === 0) {
    alert("注册成功，点击确定跳到登录界面");
    location.href = "./login.html";
  }
};
